# 支持多车的 robot_ws
带有命名空间隔离和 tf 树隔离的 robot_ws

## 命名空间隔离

在 `.bashrc` 中添加环境变量如下。

```bash
export ROBOT_NAME=slave
export ROS_NAMESPACE=slave
```

其中 `ROS_NAMESPACE` 变量是必须的，`ROBOT_NAME` 是为了便于在 `.launch` 文件中导入机器人的名称变量。

然后，从 `robot_navigation.launch` 文件开始，递归搜索涉及的 `launch` 文件和源代码文件，将 ROS 中所有绝对路径如 `/map` 修改为相对路径如 `map`。**具体修改的文件可查看 commit 历史**。

修改 `ROS_MASTER_URI` 为期望的主机。

运行 `rqt_graph`, 假设 `.bashrc` 中 `ROS_NAMESPACE=slave`, 若修改完成，在新的机器人上运行 `robot_navigation.launch` 时，会产生如下话题：

```bash
rostopic list

/diagnostics
/rosout
/rosout_agg
/slave/accel/filtered
/slave/amcl/parameter_descriptions
/slave/amcl/parameter_updates
/slave/amcl_pose
/slave/battery
/slave/cmd_vel
/slave/feedback_vel
/slave/imu_data
/slave/initialpose
/slave/joint_states
/slave/map
/slave/map_metadata
/slave/move_base/NavfnROS/plan
/slave/move_base/TebLocalPlannerROS/global_plan
/slave/move_base/TebLocalPlannerROS/local_plan
/slave/move_base/TebLocalPlannerROS/obstacles
/slave/move_base/TebLocalPlannerROS/parameter_descriptions
/slave/move_base/TebLocalPlannerROS/parameter_updates
/slave/move_base/TebLocalPlannerROS/teb_feedback
/slave/move_base/TebLocalPlannerROS/teb_markers
/slave/move_base/TebLocalPlannerROS/teb_poses
/slave/move_base/TebLocalPlannerROS/via_points
/slave/move_base/cancel
/slave/move_base/current_goal
/slave/move_base/feedback
/slave/move_base/global_costmap/costmap
/slave/move_base/global_costmap/costmap_updates
/slave/move_base/global_costmap/footprint
/slave/move_base/global_costmap/inflation_layer/parameter_descriptions
/slave/move_base/global_costmap/inflation_layer/parameter_updates
/slave/move_base/global_costmap/obstacle_layer/clearing_endpoints
/slave/move_base/global_costmap/obstacle_layer/parameter_descriptions
/slave/move_base/global_costmap/obstacle_layer/parameter_updates
/slave/move_base/global_costmap/parameter_descriptions
/slave/move_base/global_costmap/parameter_updates
/slave/move_base/global_costmap/static_layer/parameter_descriptions
/slave/move_base/global_costmap/static_layer/parameter_updates
/slave/move_base/goal
/slave/move_base/local_costmap/costmap
/slave/move_base/local_costmap/costmap_updates
/slave/move_base/local_costmap/footprint
/slave/move_base/local_costmap/inflation_layer/parameter_descriptions
/slave/move_base/local_costmap/inflation_layer/parameter_updates
/slave/move_base/local_costmap/obstacle_layer/parameter_descriptions
/slave/move_base/local_costmap/obstacle_layer/parameter_updates
/slave/move_base/local_costmap/parameter_descriptions
/slave/move_base/local_costmap/parameter_updates
/slave/move_base/parameter_descriptions
/slave/move_base/parameter_updates
/slave/move_base/result
/slave/move_base/status
/slave/move_base_simple/goal
/slave/odom
/slave/odometry/filtered
/slave/particlecloud
/slave/rpi_info
/slave/scan
/slave/scan_filtered
/slave/set_pose
/slave/startOrStop
/statistics
/tf
/tf_static
```

将产生以下节点：

```bash
/rosout
/slave/amcl
/slave/base_control
/slave/base_to_camera
/slave/base_to_gyro
/slave/base_to_laser
/slave/base_to_link
/slave/ekf_se
/slave/joint_state_publisher
/slave/laser_filter
/slave/ls01d
/slave/map_server
/slave/move_base
/slave/robot_state_publisher
```

## tf 树隔离

暂时没有找到令 tf 树使用相对路径的方法。因此，需要对参数名中带有 `frame` 的参数添加 `ROBOT_NAME/` 前缀，并对接收 `tf_prefix` 参数的节点传入 `ROBOT_NAME`。

修改完成的 `rosrun tf view_frames` 命令输出的 tf 树结构如文件 `new_slave_frames.pdf` 所示。

## 优化

本仓库涉及的改动很可能并不是最小的。

## 可能有用的动作
- 修改 `/etc/hostname` 和 `hosts` 中的名称。
- 完成所有修改后，执行 `catkin_make`。

## 版权说明

 **该仓库中代码来自 ROS 社区以及北方工业大学 Talos 实验室，如有侵权请联系删除。** 
